# Specify the Docker image to use as a base:
FROM debian:bullseye

ENV DEBIAN_FRONTEND=noninteractive

# DEBIAN PACKAGES NEEDED
# wget: needed to download Linux ISO
# rsync: needed for remastering and uploading
# squashfs-tools: needed for remastering
# genisoimage: needed for remastering
# syslinux-utils: needed for isohybrid
# bind9: for Internet connection in chroot

RUN sed -i '/deb-src/d' /etc/apt/sources.list && \
    apt-get update && apt-get upgrade -y && \
    apt-get install -y wget rsync squashfs-tools genisoimage syslinux-utils bind9

RUN useradd -m -s /bin/bash -u 1000 winner
USER winner

WORKDIR /home/winner/myapp
